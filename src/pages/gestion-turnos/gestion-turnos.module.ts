import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GestionTurnosPage } from './gestion-turnos';

@NgModule({
  declarations: [
    GestionTurnosPage,
  ],
  imports: [
    IonicPageModule.forChild(GestionTurnosPage),
  ],
})
export class GestionTurnosPageModule {}
