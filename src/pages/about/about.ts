import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  sucursales: AngularFirestoreCollection <any>;
  sucursales$: any;
  sucursalSeleccionada : any;

  franjaActual : AngularFirestoreCollection<any>;
  franjaActual$ : any;
  turnoIrAhora : AngularFirestoreCollection<any>;
  turnoIrAhora$ : any;
  filaActual: any;
  mensajeIrAhora : string;
  mensajeTurnoComunAhora : string;

  constructor(public navCtrl: NavController, private afs: AngularFirestore) {
    
  }

  ionViewDidLoad() {
    
    this.sucursales = this.afs.collection('sucursales', ref => ref.orderBy('nombre'));

    this.sucursales.valueChanges().subscribe( v =>{
    this.sucursales$ = v;
    console.log(this.sucursales$)
    });       
  }

	seleccionSucursal( $event){
    this.sucursalSeleccionada = $event.trim();
  }

	irAhora(){
		
    let dateAux = new Date();
    this.franjaActual = this.afs.collection('franjaInfo', ref => ref.where('inicio', '==', dateAux.getHours()).where('sucursal', '==', this.sucursalSeleccionada ).limit(1));
    this.franjaActual.snapshotChanges()
    .subscribe(v => {
      this.franjaActual$ = v;      
      this.turnoIrAhora = this.afs.collection('sucursales', ref => ref.where('nombre', '==', this.sucursalSeleccionada).limit(1));
      this.turnoIrAhora.snapshotChanges()
      .subscribe(v => {
        this.turnoIrAhora$ = v;      
        this.filaActual = this.turnoIrAhora$[0].payload.doc.data().cola.length;
        this.mensajeIrAhora = "En este momento hay: " + this.filaActual 
                              + " turnos, el tuyo se atendería en aproximadamente "
                              + Math.round(this.franjaActual$[0].payload.doc.data().servicio * (1/60) * this.filaActual)
                              + " minutos";
        
      });
    });	
	}  

  encolar( ){
    
    let cod = "C" + (Math.trunc(Math.random() * (999 - 100) + 100));
    let antiguaCola : any [] = this.turnoIrAhora$[0].payload.doc.data().cola;
    antiguaCola.push( cod );
    //console.log( this.turnoIrAhora$[0].payload.doc.id)
     this.turnoIrAhora.doc( this.turnoIrAhora$[0].payload.doc.id ).update({
       cola : antiguaCola
       })
    this.mensajeTurnoComunAhora = "Tu turno es: " + cod
  }
}
